
// ACTIVITY

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

// PART 1

function addUsers(nameOfUser){
    users[users.length++] = nameOfUser
}

addUsers("John Cena");
console.log(users);

// PART 2

function showUser(indexOfUser){
    displayUser = users[indexOfUser];
    console.log(displayUser);
};
itemFound = showUser(2);
itemFound = showUser(4);

// PART 3

function deleteUser(){
    let delUser = users[users.length - 1]
    users.pop();
    return delUser;
};

let delUser = deleteUser();
console.log(users);

// PART 4

function updateUser(newUser, indexNumber){
    users[indexNumber] = newUser;
}

updateUser("Triple H" , 3);
console.log(users);


// PART 5

function removeAllUsers(){
    users = [];
}

removeAllUsers();
console.log(users);

// PART 6

function isArrayEmpty(){
    if(users.length > 0){
        return false
    } else {
        return true
    }
}

let isUsersEmpty = isArrayEmpty();